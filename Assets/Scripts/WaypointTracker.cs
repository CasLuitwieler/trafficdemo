﻿using UnityEngine;

public class WaypointTracker : MonoBehaviour
{
    [SerializeField] private float _waypointDetectionRange = 0.2f;

    public BaseWaypoint TargetWaypoint { get; private set; }

    public void SetTargetWaypoint(BaseWaypoint targetWaypoint)
    {
        TargetWaypoint = targetWaypoint;
    }

    public bool HasReachedPosition(Vector3 targetPosition)
    {
        ///TODO: Subtract half the vehicle length from the target position, so that busses don't move inside other cars
        if (Vector3.Distance(transform.position, targetPosition) > _waypointDetectionRange) { return false; }
        return true;
    }
}
