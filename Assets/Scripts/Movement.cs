﻿using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField]
    private float _maxSpeed = 50f;

    public void MoveTowardsPosition(Vector3 targetPosition)
    {
        Vector3 direction = Vector3.Normalize(targetPosition - transform.position);
        transform.position += direction * _maxSpeed * Time.fixedDeltaTime;
    }

    public void MoveToPosition(Vector3 position)
    {
        transform.position = position;
    }

    public void RotateTowardsPosition(Vector3 targetPosition)
    {
        transform.LookAt(targetPosition);
    }
}
