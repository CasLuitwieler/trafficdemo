﻿using UnityEngine;

public class VehicleSpawner : MonoBehaviour
{
    [SerializeField] private BaseWaypoint _startWaypoint = null;
    [SerializeField] private GameObject _vehiclePrefab = null, _vehicleContainer = null;

    private int count = 0;

    private void SpawnVehicle()
    {
        count++;
        GameObject vehicle = Instantiate(_vehiclePrefab, _startWaypoint.transform.position, Quaternion.identity, _vehicleContainer.transform);
        vehicle.name = "vehicle" + count;
        vehicle.GetComponent<VehicleController>().SetInitialWaypoint(_startWaypoint);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            SpawnVehicle();
        }
    }
}
