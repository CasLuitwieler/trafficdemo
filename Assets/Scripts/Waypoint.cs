﻿using System;
using UnityEngine;

public class Waypoint : BaseWaypoint
{
    [SerializeField] private BaseWaypoint _nextWaypoint = null;

    public override void VehicleArrived(VehicleController vehicle)
    {
        base.VehicleArrived(vehicle);
    }

    protected override bool VehiclesCanPass()
    {
        return true;
    }

    protected override BaseWaypoint GetNextWaypoint()
    {
        return _nextWaypoint;
    }

    public override void OnDrawGizmos()
    {
        base.OnDrawGizmos();

        if (_nextWaypoint == null) { return; }

        Gizmos.DrawLine(transform.position, _nextWaypoint.AvailablePosition);
    }
}
