﻿using UnityEngine;

public class TrafficLightWaypoint : BranchingWaypoint
{
    public TrafficLightState TrafficLightState { get; private set; }
    private Color _trafficLightColor = Color.red;

    public void SetTrafficLightState(TrafficLightState trafficLightState)
    {
        TrafficLightState = trafficLightState;
        //update the traffic light color
        _trafficLightColor = trafficLightState == TrafficLightState.Green ? Color.green : Color.red;
    }

    public override void VehicleArrived(VehicleController vehicle)
    {
        base.VehicleArrived(vehicle);
    }

    protected override bool VehiclesCanPass()
    {
        return TrafficLightState == TrafficLightState.Green;
    }

    public override void OnDrawGizmos()
    {
        _gizmosColor = _trafficLightColor;
        base.OnDrawGizmos();
    }
}