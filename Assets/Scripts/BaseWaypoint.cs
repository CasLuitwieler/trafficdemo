﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseWaypoint : MonoBehaviour
{
    [SerializeField] private float _minimumVehicleDistance = 1.5f;

    //gizmos properties
    [SerializeField] private float _gizmosRadius = 0.5f;
    [SerializeField] protected Color _gizmosColor = Color.white;

    public Vector3 AvailablePosition { get; private set; }
    public Vector3 AvailableQueuePosition { get; private set; }

    private ChangeTrackWrapper<bool> _vehiclesCanPass;

    private bool _acceleratingVehicle;
    private Queue<VehicleController> _vehicles = new Queue<VehicleController>();

    private void Awake()
    {
        AvailablePosition = transform.position;
        AvailableQueuePosition = transform.position;
        _vehiclesCanPass = new ChangeTrackWrapper<bool>(false, CanPassChanged);
    }

    public virtual void VehicleArrived(VehicleController vehicle)
    {
        if (!_vehiclesCanPass.Value && vehicle.State == VehicleStates.Accelerate)
        {
            Debug.Log(vehicle.gameObject.name + " was accelerating and was stopped again by the waypoint");
            EnqueueVehicle(vehicle);
            AvailableQueuePosition = AvailablePosition;
            return;
        }
        if (!_vehiclesCanPass.Value && vehicle.State == VehicleStates.Move)
        {
            Debug.Log(vehicle.gameObject.name + " was not in queue, but was stopped by a waypoint");
            EnqueueVehicle(vehicle);
            return;
        }
        if (_vehiclesCanPass.Value && vehicle.State == VehicleStates.Accelerate)
        {
            Debug.Log(vehicle.gameObject.name + " was accelerating from queue and passed a waypoint");
            vehicle.State = VehicleStates.Move;
            vehicle.SetNewWaypoint(GetNextWaypoint());
            return;
        }
        else if (_vehiclesCanPass.Value && vehicle.State == VehicleStates.Move)
        {
            Debug.Log(vehicle.gameObject.name + " was not in queue and passed a waypoint");
            vehicle.SetNewWaypoint(GetNextWaypoint());
        }
    }

    private void Update()
    {
        _vehiclesCanPass.Value = VehiclesCanPass();

        if(_vehiclesCanPass.Value && _vehicles.Count > 0 && !_acceleratingVehicle)
        {
            StartCoroutine(AccelerateVehicle());
        }
    }

    private void EnqueueVehicle(VehicleController vehicle)
    {
        vehicle.State = VehicleStates.InQueue;
        _vehicles.Enqueue(vehicle);
        UpdateAvaiablePosition(vehicle.VehicleData, true);
    }

    private IEnumerator AccelerateVehicle()
    {
        _acceleratingVehicle = true;
        yield return new WaitForSeconds(UnityEngine.Random.Range(0.1f, 0.15f));
        VehicleController acceleratingVehicle = _vehicles.Dequeue();
        acceleratingVehicle.State = VehicleStates.Accelerate;
        UpdateAvaiablePosition(acceleratingVehicle.VehicleData, false);
        _acceleratingVehicle = false;
    }

    private void UpdateAvaiablePosition(VehicleData vehicle, bool carAddedToQueue)
    {
        Vector3 direction = Vector3.Normalize(vehicle.PreviousWaypoint.transform.position - transform.position);
        if (!carAddedToQueue) direction *= -1;
        Vector3 offset = direction * ((vehicle.Length/2) + _minimumVehicleDistance + UnityEngine.Random.Range(0.5f, 1f));
        AvailablePosition = AvailablePosition + offset;
    }

    private void CanPassChanged(bool canPass)
    {
        if (canPass)
        {
            AvailableQueuePosition = transform.position;
        }
        else
        {
            AvailablePosition = transform.position;
        }
    }

    protected abstract bool VehiclesCanPass();
    protected abstract BaseWaypoint GetNextWaypoint();

    public virtual void OnDrawGizmos()
    {
        Gizmos.color = _gizmosColor;
        Gizmos.DrawSphere(transform.position, _gizmosRadius);
    }

    public virtual void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawCube(AvailableQueuePosition, Vector3.one * 0.5f);
    }
}
