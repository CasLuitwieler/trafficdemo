﻿using UnityEngine;

public class VehicleController : MonoBehaviour
{
    [SerializeField] private float _vehicleLength = 2.5f;

    public VehicleStates State;
    public VehicleData VehicleData { get; private set; }
    public bool CanMove { get; set; }
    public bool InQueue { get; set; } = false;

    private WaypointTracker _waypointTracker;
    private Movement _moveController;

    private void Awake()
    {
        _waypointTracker = GetComponent<WaypointTracker>();
        _moveController = GetComponent<Movement>();
    }

    public void SetInitialWaypoint(BaseWaypoint waypoint)
    {
        VehicleData = new VehicleData(null, _vehicleLength);
        _waypointTracker.SetTargetWaypoint(waypoint);
        State = VehicleStates.Move;
        CanMove = true;
    }

    public void SetNewWaypoint(BaseWaypoint waypoint)
    {
        VehicleData.PreviousWaypoint = _waypointTracker.TargetWaypoint;
        _waypointTracker.SetTargetWaypoint(waypoint);
    }

    private void Update()
    {
        if (State == VehicleStates.Stop || State == VehicleStates.InQueue) { return; }

        Vector3 targetPosition = Vector3.zero;

        switch (State)
        {
            case VehicleStates.Move:
                targetPosition = _waypointTracker.TargetWaypoint.AvailablePosition;
                break;
            case VehicleStates.Accelerate:
                targetPosition = _waypointTracker.TargetWaypoint.AvailableQueuePosition;
                break;
        }

        if (_waypointTracker.HasReachedPosition(targetPosition))
        {
            //move vehicle to exact position
            _moveController.MoveToPosition(targetPosition);

            _waypointTracker.TargetWaypoint.VehicleArrived(this);
        }
    }

    private void FixedUpdate()
    {
        if (State == VehicleStates.Stop || State == VehicleStates.InQueue) { return; }

        Vector3 targetPosition = Vector3.zero;

        switch (State)
        {
            case VehicleStates.Move:
                targetPosition = _waypointTracker.TargetWaypoint.AvailablePosition;
                break;
            case VehicleStates.Accelerate:
                targetPosition = _waypointTracker.TargetWaypoint.AvailableQueuePosition;
                break;
        }

        _moveController.MoveTowardsPosition(targetPosition);
        _moveController.RotateTowardsPosition(targetPosition);
    }
}

public enum VehicleStates
{
    Move,
    Stop,
    InQueue,
    Accelerate,
    None
}
