﻿using System;
using System.Collections;
using UnityEngine;

public class WorldGenerator : MonoBehaviour
{
    [SerializeField] private GameObject _tilePrefab = null;

    public Tile[,] Tiles { get; private set; }

    [SerializeField] private Vector2 _tileSize = Vector2.zero;
    [SerializeField] private Vector2 _WorldSize = Vector2.zero;

    private float _halfWorldX, _halfWorldY;

    private void Awake()
    {
        GenerateWorld();
        _halfWorldX = _WorldSize.x * _tileSize.x / 2f;
        _halfWorldY = _WorldSize.y * _tileSize.y / 2f;
    }

    private void GenerateWorld()
    {
        Tiles = new Tile[(int)_WorldSize.x, (int)_WorldSize.y];

        for(int y = 0; y < _WorldSize.y; y++)
        {
            for (int x = 0; x < _WorldSize.x; x++)
            {
                Vector3 spawnPosition = new Vector3(-_halfWorldX + (x * _tileSize.x), -_halfWorldY + (y * _tileSize.y));
                Tiles[x, y] = Instantiate(_tilePrefab, spawnPosition, Quaternion.identity).GetComponent<Tile>();
            }
        }
    }
}
