﻿using System.Collections;
using UnityEngine;

public class TrafficLightsController : MonoBehaviour
{
    [SerializeField] private float _trafficLightDuration = 3f;
    [SerializeField] private TrafficLightWaypoint[] _trafficLights = null;

    private int nTrafficLights;

    private void Awake()
    {
        if(_trafficLights == null) { Debug.LogWarning("No traffic lights added to traffic light controller"); return; }
        nTrafficLights = _trafficLights.Length;

        ResetTrafficLights();
        StartCoroutine(CycleTrafficLights());
    }

    private IEnumerator CycleTrafficLights()
    {
        int currentGreenLight = 1, previousGreenLight = 0;
        while(true)
        {
            //Debug.Log("previousGreenLight: " + previousGreenLight + "\t" + "currentGreenLight: " + currentGreenLight);
            _trafficLights[previousGreenLight].SetTrafficLightState(TrafficLightState.Red);
            _trafficLights[currentGreenLight].SetTrafficLightState(TrafficLightState.Green);
            yield return new WaitForSeconds(_trafficLightDuration);
            currentGreenLight = ++currentGreenLight % nTrafficLights;
            previousGreenLight = ++previousGreenLight % nTrafficLights;
        }
    }

    private void ResetTrafficLights()
    {
        foreach (TrafficLightWaypoint trafficLight in _trafficLights)
        {
            trafficLight.SetTrafficLightState(TrafficLightState.Red);
        }
    }
}

public enum TrafficLightState
{
    Green,
    Red,
    None
}