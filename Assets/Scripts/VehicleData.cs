﻿public class VehicleData
{
    public BaseWaypoint PreviousWaypoint { get; set; }
    public float Length { get; private set; }
    
    public VehicleData(BaseWaypoint previousWaypoint, float length)
    {
        PreviousWaypoint = previousWaypoint;
        Length = length;
    }
}
