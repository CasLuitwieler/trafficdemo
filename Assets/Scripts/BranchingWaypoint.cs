﻿using UnityEngine;

public class BranchingWaypoint : BaseWaypoint
{
    [SerializeField] private BaseWaypoint[] _possibleWaypoints = null;
    [SerializeField] private bool _useRandomDistribution = false;

    private BaseWaypoint GetRandomWaypoint()
    {
        if (_possibleWaypoints == null) return null;
        BaseWaypoint randomWaypoint = _possibleWaypoints[Random.Range(0, _possibleWaypoints.Length)];
        return randomWaypoint;
    }

    ///TODO: null check _possibleWaypoints[] and make the random distribution configurable
    private BaseWaypoint GetRandomDistributedWaypoint()
    {
        if (_possibleWaypoints == null) return null;

        int randomNumber = Random.Range(0, 100);
        if (randomNumber < 20)
        {
            return _possibleWaypoints[0];
        }
        else if (randomNumber < 40)
        {
            return _possibleWaypoints[1];
        }
        else
        {
            return _possibleWaypoints[2];
        }
    }

    public override void VehicleArrived(VehicleController vehicle)
    {
        base.VehicleArrived(vehicle);
    }

    protected override bool VehiclesCanPass()
    {
        return true;
    }

    protected override BaseWaypoint GetNextWaypoint()
    {
        if (_useRandomDistribution)
            return GetRandomDistributedWaypoint();
        return GetRandomWaypoint();
    }

    public override void OnDrawGizmosSelected()
    {
        base.OnDrawGizmosSelected();
        Gizmos.color = Color.blue;
        foreach (BaseWaypoint waypoint in _possibleWaypoints)
        {
            Gizmos.DrawLine(transform.position, waypoint.transform.position);
        }
    }
}