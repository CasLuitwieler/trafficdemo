﻿using System;

public class ChangeTrackWrapper<T>
{
    public Action<T> OnValueChanged;

    private T _value;
    public T Value
    {
        get { return _value; }
        set
        {
            if (value.Equals(_value)) { return; }
            _value = value;
            _hasChanged = true;
            OnValueChanged?.Invoke(_value);
        }
    }

    public ChangeTrackWrapper()
    {
    }

    public ChangeTrackWrapper(T value, Action<T> onValueChanged)
    {
        _value = value;
        if (onValueChanged != null)
            OnValueChanged += onValueChanged;
    }

    private bool _hasChanged = false;

    public bool HasChanged()
    {
        return _hasChanged;
    }

    public void ResetChangeFlag()
    {
        _hasChanged = false;
    }
}
